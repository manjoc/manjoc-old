import {Express} from "express"

import initApi from "./api"
import User from "./database/entities/user"

let testApi: Express | null = null
let testUser: User | null = null

async function initTestApi() {
  if (!testApi) {
    testApi = await initApi()
  }

  // create test user
  User.clear()
  testUser = User.create({
    email: "test@gmail.com",
    username: "test",
    password: "testpwd",
  })
  await testUser.save()
}

export {initTestApi, testApi, testUser}
