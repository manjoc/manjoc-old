import express from "express"
import {createConnection} from "typeorm"

import {JWTMidlleware, UserMidlleware} from "./modules/auth/middlewares"
import auth from "./modules/auth/router"
import users from "./modules/users/router"

const {API_PORT, IS_TEST} = process.env

async function initApi() {
  await createConnection()

  const app = express()

  // express middlewares
  app.use(express.json())

  // custom middlewares
  app.use(JWTMidlleware, UserMidlleware)

  // routers
  app.use("/auth", auth)
  app.use("/users", users)

  app.listen(
    API_PORT,
    () => !IS_TEST && console.log(`listening at http://localhost:${API_PORT}`)
  )

  return app
}

export default initApi
