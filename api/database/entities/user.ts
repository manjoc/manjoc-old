import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  Unique,
} from "typeorm"

import filterObject from "../../utils/misc"
@Entity()
@Unique(["email"])
class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  username: string

  @Column()
  password: string

  @Column()
  email: string

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date

  static UPDATABLE_FIELDS: (keyof User)[] = ["email", "username"]

  update(changes: Partial<User>) {
    const filteredChanges = filterObject(changes, User.UPDATABLE_FIELDS)
    Object.assign(this, filteredChanges)
    return this.save()
  }

  // Pas de @ class-validator pour l'instant, donc pas besoin de la fonction
  // @BeforeInsert()
  // @BeforeUpdate()
  // async validateEntity() {
  //   try {
  //     await validateOrReject(this)
  //   } catch (errors) {
  //     throw new EntityValidationErrors("user validation error", errors)
  //   }
  // }
}

export default User
