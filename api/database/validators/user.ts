import {ParamSchema} from "express-validator"

class UserValidators {
  static password(otherParams?: ParamSchema): ParamSchema {
    return {
      isString: {
        errorMessage: "password should be a string",
      },
      isLength: {
        errorMessage: "password should be at least 6 chars long",
        options: {min: 6},
      },
      escape: true,
      trim: true,
      ...otherParams,
    }
  }

  static email(otherParams?: ParamSchema): ParamSchema {
    return {
      isEmail: {
        errorMessage: "email should be valid",
      },
      normalizeEmail: true,
      ...otherParams,
    }
  }

  static username(otherParams?: ParamSchema): ParamSchema {
    return {
      isString: {
        errorMessage: "username should be a string",
      },
      isLength: {
        errorMessage: "username should be at least 3 chars long",
        options: {min: 3},
      },
      escape: true,
      trim: true,
      ...otherParams,
    }
  }
}

export default UserValidators
