import {Router} from "express"

import User from "../../database/entities/user"
import HTTPError from "../../utils/http/error-reponses"
import HTTPSuccess from "../../utils/http/success-responses"
import {HTTPRequest, HTTPResponse} from "../../utils/http/types"
import {serializeMinimalUser, SMinimalUser} from "./serializers"

const router: Router = Router()

router.get("/", async (_req, res: HTTPResponse<SMinimalUser[]>) => {
  const users = await User.find()

  return HTTPSuccess(
    res,
    users.map((user: User) => serializeMinimalUser(user))
  )
})

router.get(
  "/:id",
  async (
    req: HTTPRequest<any, {id: string}>,
    res: HTTPResponse<SMinimalUser>
  ) => {
    const user = await User.findOne(req.params.id)

    if (!user) return HTTPError(res, `no such user`, null)

    return HTTPSuccess(res, serializeMinimalUser(user))
  }
)

export default router
