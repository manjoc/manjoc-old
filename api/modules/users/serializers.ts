import User from "../../database/entities/user"
import serialize, {Serialized, Fields} from "../../utils/serialize"
import {JWTPayload} from "../../utils/http/types"

export interface SMinimalUser extends Serialized<"minimal"> {
  id: number
  username: string
  email: string
}

export function serializeMinimalUser(user: User): SMinimalUser {
  const ALLOWED_FIELDS: Fields<SMinimalUser> = ["id", "username", "email"]
  return serialize(user, ALLOWED_FIELDS, "minimal")
}

export interface SFullUser extends Serialized<"full"> {
  id: number
  username: string
  password: string
  email: string
  created_at: Date
  updated_at: Date
}

export function serializeFullUser(user: User): SFullUser {
  const ALLOWED_FIELDS: Fields<SFullUser> = [
    "id",
    "username",
    "email",
    "created_at",
    "updated_at",
  ]

  return serialize(user, ALLOWED_FIELDS, "full")
}

type SJWTUser = JWTPayload & Serialized<"jwt">

export function serializeJWTUser(user: User): SJWTUser {
  const ALLOWED_FIELDS: Fields<SJWTUser> = ["id", "username"]

  return serialize(user, ALLOWED_FIELDS, "jwt")
}
