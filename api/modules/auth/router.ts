import {Router} from "express"
import {QueryFailedError} from "typeorm"
import jwt from "jsonwebtoken"
import argon2 from "argon2"

import User from "../../database/entities/user"
import {
  SFullUser,
  serializeFullUser,
  serializeJWTUser,
} from "../users/serializers"
import UserValidators from "../../database/validators/user"
import UnreachableError from "../../utils/exceptions/unreachable-error"
import EntityValidationErrors from "../../utils/exceptions/entity-validation-error"
import HTTPError from "../../utils/http/error-reponses"
import HTTPSuccess from "../../utils/http/success-responses"
import {validateRequest} from "../../utils/http/middlewares"
import {HTTPRequest, HTTPResponse, RequestSchema} from "../../utils/http/types"
import {OnlyLoggedIn, OnlyLoggedOut} from "../auth/guards"
import TokenBlacklist from "./token-blacklist"
import {matchedData} from "express-validator"

const {SECRET_JWT_TOKEN} = process.env

const router: Router = Router()

interface LoginReq {
  email: string
  password: string
}

const loginReqSchema: RequestSchema<LoginReq> = {
  email: UserValidators.email(),
  password: UserValidators.password(),
}

router.post(
  "/login",
  OnlyLoggedOut,
  validateRequest(loginReqSchema),
  async (
    req: HTTPRequest<LoginReq>,
    res: HTTPResponse<{token: string; user: SFullUser}>
  ) => {
    // extract only data precised on the request schema
    const data = matchedData(req)

    const user = await User.findOne({where: {email: data.email}})

    // does user exists?
    if (!user) return HTTPError(res, "user does not exists", null)

    // verify password
    if (!(await argon2.verify(user.password, data.password)))
      return HTTPError(res, "password invalid", null)

    // generate a new JWT token
    const token = jwt.sign(serializeJWTUser(user), SECRET_JWT_TOKEN!, {
      expiresIn: "6h",
    })

    return HTTPSuccess(res, {token, user: serializeFullUser(user)})
  }
)

router.post(
  "/logout",
  OnlyLoggedIn,
  async (req: HTTPRequest, res: HTTPResponse) => {
    if (!req.rawjwt) throw new UnreachableError("req.rawjwt must be defined")

    TokenBlacklist.add(req.rawjwt)

    return HTTPSuccess(res, null)
  }
)

router.get(
  "/current",
  OnlyLoggedIn,
  async (req: HTTPRequest, res: HTTPResponse<SFullUser>) => {
    if (!req.user) throw new UnreachableError("req.user must be defined")
    return HTTPSuccess(res, serializeFullUser(req.user))
  }
)

type UpdateReq = Pick<Partial<User>, "email" | "username">

const UpdateReqSchema: RequestSchema<UpdateReq> = {
  email: UserValidators.email({optional: true}),
  username: UserValidators.username({optional: true}),
}

router.put(
  "/current",
  OnlyLoggedIn,
  validateRequest(UpdateReqSchema),
  async (
    req: HTTPRequest<Partial<User>>,
    res: HTTPResponse<{token: string; user: SFullUser}>
  ) => {
    if (!req.user) throw new UnreachableError("req.user must be defined")

    let user = await User.findOne(req.user.id)

    if (!user) throw new UnreachableError("req.user.id must be valid")

    // extract only data precised on the request schema
    const data = matchedData(req)

    try {
      user = await user.update(data)
    } catch (error) {
      if (error instanceof EntityValidationErrors) {
        return HTTPError(res, error.message, error.errors)
      }
    }

    return HTTPSuccess(res, null)
  }
)

router.delete(
  "/current",
  OnlyLoggedIn,
  async (req: HTTPRequest, res: HTTPResponse<SFullUser>) => {
    if (!req.user) throw new UnreachableError("req.user must be defined")
    if (!req.rawjwt) throw new UnreachableError("req.rawjwt must be defined")

    TokenBlacklist.remove(req.rawjwt!)
    await User.remove(req.user)

    return HTTPSuccess(res, null)
  }
)

interface RegisterReq {
  email: string
  password: string
  username: string
}

const registerReqSchema: RequestSchema<RegisterReq> = {
  email: UserValidators.email(),
  password: UserValidators.password(),
  username: UserValidators.username(),
}

router.post(
  "/register",
  OnlyLoggedOut,
  validateRequest(registerReqSchema),
  async (
    req: HTTPRequest<RegisterReq>,
    res: HTTPResponse<{token: string; user: SFullUser}>
  ) => {
    // extract only data precised on the request schema
    const data = matchedData(req)

    // hash password
    const hashedPwd = await argon2.hash(data.password)

    // create the user
    const user = User.create({...data, password: hashedPwd})

    try {
      await user.save()
    } catch (error) {
      if (error instanceof EntityValidationErrors) {
        return HTTPError(res, error.message, error.errors)
      } else if (error instanceof QueryFailedError) {
        return HTTPError(res, "query failed error", (error as any).detail)
      }
    }

    // generate a new JWT token
    const token = jwt.sign(serializeJWTUser(user), SECRET_JWT_TOKEN!, {
      expiresIn: "6h",
    })

    return HTTPSuccess(res, {token, user: serializeFullUser(user)})
  }
)

export default router
