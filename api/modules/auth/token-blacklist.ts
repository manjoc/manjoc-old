class TokenBlacklist {
  private tokens: string[] = []

  contains(token: string) {
    return this.tokens.includes(token)
  }

  add(token: string) {
    if (this.contains(token)) return

    this.tokens.push(token)
  }

  remove(token: string) {
    const index = this.tokens.indexOf(token, 0)

    if (index !== -1) this.tokens.splice(index, 1)
  }

  clear() {
    this.tokens = []
  }
}

export default new TokenBlacklist()
