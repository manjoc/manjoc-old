import {NextFunction} from "express"

import {HTTPUnauthorized} from "../../utils/http/error-reponses"
import {HTTPRequest, HTTPResponse} from "../../utils/http/types"

export function OnlyLoggedIn(
  req: HTTPRequest,
  res: HTTPResponse,
  next: NextFunction
) {
  if (!req.user) return HTTPUnauthorized(res, "req.user must be defined")

  next()
}

export function OnlyLoggedOut(
  req: HTTPRequest,
  res: HTTPResponse,
  next: NextFunction
) {
  if (req.user) return HTTPUnauthorized(res, "req.user is defined")

  next()
}
