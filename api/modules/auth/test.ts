import chai from "chai"
import chaiHttp from "chai-http"
import jwt from "jsonwebtoken"
import argon2 from "argon2"

import {testApi, initTestApi} from "../../testapi"
import User from "../../database/entities/user"
import TokenBlacklist from "./token-blacklist"
import {JWTPayload} from "../../utils/http/types"
import {
  verifyErrorResponse,
  verifySuccesResponse,
  verifyGuardOnlyLoggedIn,
  verifyGuardOnlyLoggedOut,
} from "../../utils/chai-utils"

const {SECRET_JWT_TOKEN} = process.env

// setup chai
chai.use(chaiHttp)
const expect = chai.expect

// setup for tests
before(async () => {
  // init the api to test
  await initTestApi()
})

// data used in tests
let JWTToken = ""
const userData = {
  username: "Bob",
  email: "bobmail@gmail.com",
  password: "bobpwd",
}

// tests
describe("Auth API", () => {
  describe("Testing routes guard :", () => {
    // onlyLoggedOut guard
    verifyGuardOnlyLoggedOut("/auth/register", "post")
    verifyGuardOnlyLoggedOut("/auth/login", "post")

    // onlyLoggedIn guard
    verifyGuardOnlyLoggedIn("/auth/current", "get")
    verifyGuardOnlyLoggedIn("/auth/current", "put")
    verifyGuardOnlyLoggedIn("/auth/current", "delete")
    verifyGuardOnlyLoggedIn("/auth/logout", "post")
  })

  describe("Testing routes :", () => {
    describe("auth/register:", () => {
      context("> with data forgiven", () => {
        it("should fail to register a user if no email provided", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/register")
            .send({username: userData.username, password: userData.password})

          expect(res.body.error.message).to.deep.equal("invalid request")
          expect(res.body.error.details).to.be.not.null
        })
        it("should fail to register a user if no password provided", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/register")
            .send({email: userData.email, username: userData.username})

          verifyErrorResponse(res)

          expect(res.body.error.message).to.deep.equal("invalid request")
          expect(res.body.error.details).to.be.not.null
        })
      })
      context("> with bad data format", () => {
        it("should fail to register a user if bad email format", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/register")
            .send({...userData, email: "bobmailgmail.com"})

          verifyErrorResponse(res)

          expect(res.body.error.message).to.deep.equal("invalid request")
          expect(res.body.error.details).to.be.not.null
        })
        it("should fail to register a user if bad password format", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/register")
            .send({...userData, password: "bad"})

          verifyErrorResponse(res)

          expect(res.body.error.message).to.deep.equal("invalid request")
          expect(res.body.error.details).to.be.not.null
        })
      })
      context("> with correct data format", () => {
        it("should register a user", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/register")
            .send(userData)

          verifySuccesResponse(res)

          const data = res.body.data

          expect(data).to.have.property("token").that.is.not.null
          expect(data).to.have.property("user").that.is.not.null

          const {token, user} = data

          // verify user
          const userOnDb = await User.findOne(user.id)
          expect(userOnDb).to.be.not.undefined
          expect(userOnDb!.email).to.be.equal(userData.email)
          expect(await argon2.verify(userOnDb!.password, userData.password)).to
            .be.true
          expect(userOnDb!.username).to.be.equal(userData.username)

          // verify token
          jwt.verify(token, SECRET_JWT_TOKEN!)
          JWTToken = token
        })
      })
    })

    describe("auth/login:", () => {
      context("> with wrong credentials", () => {
        it("should fail to login if wrong email", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/login")
            .send({email: "boblol@gmail.com", password: userData.password})

          verifyErrorResponse(res)

          expect(res.body.error.message).to.deep.equal("user does not exists")
        })
        it("should fail to login if wrong password", async function () {
          const res = await chai
            .request(testApi)
            .post("/auth/login")
            .send({email: userData.email, password: "boblolpwd"})

          verifyErrorResponse(res)

          expect(res.body.error.message).to.deep.equal("password invalid")
        })
      })
      context("> with correct credentials", () => {
        it("should login", async function () {
          let res = await chai
            .request(testApi)
            .post("/auth/login")
            .send(userData)

          verifySuccesResponse(res)

          const data = res.body.data

          expect(data).to.have.property("token").that.is.not.null
          expect(data).to.have.property("user").that.is.not.null

          const {token, user} = data

          // verify user
          const userOnDb = await User.findOne(user.id)
          expect(userOnDb).to.be.not.undefined
          expect(userOnDb!.email).to.be.equal(userData.email)
          expect(await argon2.verify(userOnDb!.password, userData.password)).to
            .be.true
          expect(userOnDb!.username).to.be.equal(userData.username)

          // verify token
          jwt.verify(token, SECRET_JWT_TOKEN!)
          JWTToken = token
        })
      })
    })

    describe("auth/logout:", () => {
      after(async () => {
        TokenBlacklist.remove(JWTToken)
      })
      it("should logout current user", async function () {
        const res = await chai
          .request(testApi)
          .post("/auth/logout")
          .set({Authorization: `Bearer ${JWTToken}`})

        verifySuccesResponse(res)

        expect(TokenBlacklist.contains(JWTToken)).to.be.true
      })
    })

    describe("auth/current (get):", () => {
      it("should return the logged user", async function () {
        const res = await chai
          .request(testApi)
          .get("/auth/current")
          .set({Authorization: `Bearer ${JWTToken}`})

        verifySuccesResponse(res)

        // verify user
        const user = res.body.data
        const userOnDb = await User.findOne(user.id)
        expect(userOnDb).to.be.not.undefined
        expect(userOnDb!.email).to.be.equal(userData.email)
        expect(await argon2.verify(userOnDb!.password, userData.password)).to.be
          .true
        expect(userOnDb!.username).to.be.equal(userData.username)
      })
    })

    describe("auth/current (put):", () => {
      context("> with bad data format", () => {
        it("should fail to update email if bad email format", async function () {
          const res = await chai
            .request(testApi)
            .put("/auth/current")
            .set({Authorization: `Bearer ${JWTToken}`})
            .send({email: "bobnewmailgmail.com"})

          verifyErrorResponse(res)

          expect(res.body.error.message).to.deep.equal("invalid request")
          expect(res.body.error.details).to.be.not.null
        })
        it("should not update unupdatable fields", async function () {
          const res = await chai
            .request(testApi)
            .put("/auth/current")
            .set({Authorization: `Bearer ${JWTToken}`})
            .send({password: "1234"})

          verifySuccesResponse(res)

          const jwtDecoded = jwt.decode(JWTToken) as JWTPayload

          // verify user update
          const userOnDb = await User.findOne(jwtDecoded.id)
          expect(userOnDb).to.be.not.undefined
          expect(await argon2.verify(userOnDb!.password, userData.password)).to
            .be.true
        })
      })
      context("> with correct data format", () => {
        it("should update current user data", async function () {
          // new user data
          const newEmail = "bobnewmail@gmail.com"
          const newUsername = "NewBob"

          const res = await chai
            .request(testApi)
            .put("/auth/current")
            .set({Authorization: `Bearer ${JWTToken}`})
            .send({email: newEmail, username: newUsername})

          verifySuccesResponse(res)

          const jwtDecoded = jwt.decode(JWTToken) as JWTPayload

          // verify user update
          const userOnDb = await User.findOne(jwtDecoded.id)
          expect(userOnDb).to.be.not.undefined
          expect(userOnDb!.email).to.be.equal(newEmail)
          expect(userOnDb!.username).to.be.equal(newUsername)
        })
      })
    })

    describe("auth/current (delete):", () => {
      it("should delete current user", async function () {
        const res = await chai
          .request(testApi)
          .delete("/auth/current")
          .set({Authorization: `Bearer ${JWTToken}`})

        verifySuccesResponse(res)

        const jwtDecoded = jwt.decode(JWTToken) as JWTPayload

        // verify user delete
        const userOnDb = await User.findOne(jwtDecoded.id)
        expect(userOnDb).to.be.undefined

        expect(TokenBlacklist.contains(JWTToken)).to.be.false
      })
    })
  })
})
