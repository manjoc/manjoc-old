import {NextFunction} from "express"
import jwt from "jsonwebtoken"

import TokenBlacklist from "./token-blacklist"
import User from "../../database/entities/user"
import {HTTPRequest, HTTPResponse, JWTPayload} from "../../utils/http/types"

const {SECRET_JWT_TOKEN} = process.env

export function JWTMidlleware(
  req: HTTPRequest,
  _res: HTTPResponse,
  next: NextFunction
) {
  // is Authorization header set?
  if (!req.headers.authorization) return next()

  // TODO: is there another way to do this?
  const authorization = req.headers.authorization.split("Bearer ")
  if (authorization.length != 2) return next()

  const token = authorization[1]

  // is token blacklisted?
  if (TokenBlacklist.contains(token)) return next()

  try {
    // check token integrity
    req.jwt = jwt.verify(token, SECRET_JWT_TOKEN!) as JWTPayload
    req.rawjwt = token

    next()
  } catch (_) {
    // invalid token
    return next()
  }
}

export async function UserMidlleware(
  req: HTTPRequest,
  _res: HTTPResponse,
  next: NextFunction
) {
  // does jwt exists?
  if (!req.jwt) return next()

  req.user = await User.findOne(req.jwt.id)

  next()
}
