import {ValidationError} from "class-validator"

export interface EntityValidationError {
  field: string
  wrongValue: string
  constraint: {[type: string]: string} | undefined
}

class EntityValidationErrors extends Error {
  errors: EntityValidationError[]

  constructor(message: string, errors: ValidationError[]) {
    super(message)
    this.name = "EntityValidationErrors"
    this.errors = errors.map((error: ValidationError) => {
      return {
        field: error.property,
        wrongValue: error.value,
        constraint: error.constraints,
      }
    })
  }
}

export default EntityValidationErrors
