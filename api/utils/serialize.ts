import filterObject from "./misc"

export default function serialize<T, K extends keyof T, S>(
  obj: T,
  allowedFields: K[],
  _type: S
): Pick<T, K> & {serializer: S} {
  return filterObject(obj, allowedFields) as Pick<T, K> & {serializer: S}
}

export interface Serialized<T> {
  serializer: T
}

export type Fields<T> = Exclude<keyof T, "serializer">[]
