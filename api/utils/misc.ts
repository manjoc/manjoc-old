export default function filterObject<T, K extends keyof T>(
  obj: T,
  allowedFields: K[]
): Pick<T, K> {
  let res: any = {}

  allowedFields.forEach(field => {
    if (obj[field]) {
      res[field] = obj[field]
    }
  })

  return res
}
