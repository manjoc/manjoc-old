import jwt from "jsonwebtoken"
import chai from "chai"
import chaiHttp from "chai-http"

import {testUser, testApi} from "../testapi"
import {serializeJWTUser} from "../modules/users/serializers"
import {HTTPMethod} from "./http/types"

const {SECRET_JWT_TOKEN} = process.env

// setup chai
chai.use(chaiHttp)
const expect = chai.expect

function verifyResponseStatus(res: any, status: number) {
  expect(res).to.have.status(status)
  expect(res.body).to.have.property("status").that.is.equals(status)
}

export function verifyErrorResponse(res: any, status = 400) {
  verifyResponseStatus(res, status)
  expect(res.body).to.have.property("error").that.is.not.null
  expect(res.body).to.have.property("data").that.is.null
  expect(res.body.error).to.have.property("message").that.is.not.null
}

export function verifySuccesResponse(res: any, status = 200) {
  verifyResponseStatus(res, status)
  expect(res.body).to.have.property("error").that.is.null
  expect(res.body).to.have.property("data")
}

export async function verifyGuardOnlyLoggedIn(
  route: string,
  httpMethod: HTTPMethod
) {
  it(`(onlyLoggedIn) ${httpMethod} on ${route}`, async () => {
    const res = await chai.request(testApi)[httpMethod](route)

    verifyErrorResponse(res, 401)
    expect(res.body.error.message).to.deep.equal("req.user must be defined")
  })
}

export async function verifyGuardOnlyLoggedOut(
  route: string,
  httpMethod: HTTPMethod
) {
  it(`(onlyLoggedOut) ${httpMethod} on ${route}`, async () => {
    let JWTToken: string

    JWTToken = jwt.sign(serializeJWTUser(testUser!), SECRET_JWT_TOKEN!, {
      expiresIn: "6h",
    })

    const res = await chai
      .request(testApi)
      [httpMethod](route)
      .set({Authorization: `Bearer ${JWTToken}`})
    verifyErrorResponse(res, 401)
    expect(res.body.error.message).to.deep.equal("req.user is defined")
  })
}
