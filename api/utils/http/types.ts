import {Request, Response} from "express"
import {Params} from "express-serve-static-core"
import User from "../../database/entities/user"
import {ParamSchema} from "express-validator"
import {SMinimalUser} from "../../modules/users/serializers"

/* Body of a HTTPResponse */
interface ResponseBody<DataType = any> {
  status: number
  error: ErrorBody | null
  data: DataType | null
}

/* Error on a ResponseBody */
interface ErrorBody {
  message: string
  //TODO: In future -> code: ErrorCode (ErrorCode will be a file with all our
  // error codes shared between api and front ) but for now code == ResponseBody.status
  code: number
  details: any
}

export interface JWTPayload {
  id: number // User id
  username: string
}

/* Express request HTTP */
export interface HTTPRequest<ReqBodyType = any, ParamsType extends Params = {}>
  extends Request<ParamsType, any, ReqBodyType> {
  jwt?: JWTPayload
  rawjwt?: string
  user?: User
}

/* Express response HTTP */
export type HTTPResponse<DataType = any> = Response<ResponseBody<DataType>>

export type HTTPMethod = "put" | "get" | "delete" | "post"

export type RequestSchema<T = undefined> = Record<
  T extends null | undefined ? string : keyof T,
  ParamSchema
>
