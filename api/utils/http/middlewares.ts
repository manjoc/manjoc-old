import {RequestSchema, HTTPRequest, HTTPResponse} from "./types"
import {NextFunction} from "express"
import {checkSchema, validationResult} from "express-validator"
import HTTPError from "./error-reponses"

export function validateRequest<T>(schema: RequestSchema<T>) {
  return [
    ...checkSchema(schema),
    (req: HTTPRequest, res: HTTPResponse, next: NextFunction) => {
      try {
        validationResult(req).throw()

        next()
      } catch (error) {
        HTTPError(res, "invalid request", error.mapped())
      }
    },
  ]
}
