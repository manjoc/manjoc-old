import {HTTPResponse} from "./types"

export default function HTTPError(
  res: HTTPResponse,
  message: string,
  details: any,
  status = 400
) {
  res.status(status)
  res.send({
    status,
    data: null,
    error: {details, message, code: status},
  })
}

export const HTTPNotFound = <T>(res: HTTPResponse<T>, message: string) =>
  HTTPError(res, message, null, 404)

export const HTTPUnauthorized = <T>(res: HTTPResponse<T>, message: string) =>
  HTTPError(res, message, null, 401)
