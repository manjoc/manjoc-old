import {HTTPResponse} from "./types"

export default function HTTPSuccess<T>(
  res: HTTPResponse<T>,
  data: T | null,
  status = 200
) {
  res.status(status)
  res.send({data, status: status, error: null})
}

export const HTTPCreated = <T>(res: HTTPResponse<T>, data: T | null) =>
  HTTPSuccess(res, data, 201)
