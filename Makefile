.PHONY: build-api dev test

build-api: api/Dockerfile api/yarn.lock
	docker-compose build

dev:
	docker-compose up

test:
	docker-compose -f docker-compose.test.yaml -p manjoc-test run api
